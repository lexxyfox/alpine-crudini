* Designed for use with automated CI pipelines.
* Currently for x86_64 only. Looking for help with other architectures.
* I'll sign my stuff when I get around to it :v

## Install:
```bash
echo https://lexxyfox.gitlab.io/alpine-crudini >> /etc/apk/repositories
apk add -U --allow-untrusted crudini
```

